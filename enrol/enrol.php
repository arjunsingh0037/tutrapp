<?php

require_once(dirname(__FILE__).'/../lms_webservice.php');

class tutor_webservice_enrol extends lms_webservice {
    /**
     * Returns description of method parameters.
     *
     * @return external_function_parameters
     * @since Moodle 2.2
     */
    public static function enrol_allusers_parameters() {
        return new external_function_parameters(
                array(
                    'enrolments' => new external_multiple_structure(
                            new external_single_structure(
                                    array(
                                        'roleid' => new external_value(PARAM_INT, 'Role to assign to the user'),
                                        'userid' => new external_value(PARAM_INT, 'The user that is going to be enrolled'),
                                        'courseid' => new external_value(PARAM_RAW, 'The course to enrol the user role in'),
                                        'timestart' => new external_value(PARAM_INT, 'Timestamp when the enrolment start', VALUE_OPTIONAL),
                                        'timeend' => new external_value(PARAM_INT, 'Timestamp when the enrolment end', VALUE_OPTIONAL),
                                        'suspend' => new external_value(PARAM_INT, 'set to 1 to suspend the enrolment', VALUE_OPTIONAL)
                                    )
                            )
                    )
                )
        );
    }

    /**
     * Enrolment of users.
     *
     * Function throw an exception at the first error encountered.
     * @param array $enrolments  An array of user enrolment
     * @since Moodle 2.2
    */
    public static function enrol_allusers($enrolments) {
        global $DB, $CFG;

        require_once($CFG->libdir . '/enrollib.php');

        $params = self::validate_parameters(self::enrol_allusers_parameters(),
                array('enrolments' => $enrolments));

        //$transaction = $DB->start_delegated_transaction(); // Rollback all enrolment if an error occurs
                                                           // (except if the DB doesn't support it).

        // Retrieve the manual enrolment plugin.
        $enrol = enrol_get_plugin('manual');
        if (empty($enrol)) {
            throw new moodle_exception('manualpluginnotinstalled', 'enrol_manual');
        }

        foreach ($params['enrolments'] as $enrolment) {
            if ($enrolment['courseid'] == 'all') {

                $allcourses = $DB->get_records('course',$condition= null,'id');
                unset ($allcourses[0]);
                foreach ($allcourses  as  $value) {
                    if($value->id != 1){
                  
                
                    $context = context_course::instance($value->id, IGNORE_MISSING);
                    //self::validate_context($context);

                    // Check that the user has the permission to manual enrol.
                    require_capability('enrol/manual:enrol', $context);

                    // Throw an exception if user is not able to assign the role.
                    $roles = get_assignable_roles($context);
                    if (!array_key_exists($enrolment['roleid'], $roles)) {
                        $errorparams = new stdClass();
                        $errorparams->roleid = $enrolment['roleid'];
                        $errorparams->courseid = $value->id;
                        $errorparams->userid = $enrolment['userid'];
                        throw new moodle_exception('wsusercannotassign', 'enrol_manual', '', $errorparams);
                    }

                    // Check manual enrolment plugin instance is enabled/exist.
                    $instance = null;
                    $enrolinstances = enrol_get_instances($value->id, true);
                    foreach ($enrolinstances as $courseenrolinstance) {
                      if ($courseenrolinstance->enrol == "manual") {
                          $instance = $courseenrolinstance;
                          break;
                      }
                    }
                    if (empty($instance)) {
                      $errorparams = new stdClass();
                      $errorparams->courseid = $value->id;
                      throw new moodle_exception('wsnoinstance', 'enrol_manual', $errorparams);
                    }

                    // Check that the plugin accept enrolment (it should always the case, it's hard coded in the plugin).
                    if (!$enrol->allow_enrol($instance)) {
                        $errorparams = new stdClass();
                        $errorparams->roleid = $enrolment['roleid'];
                        $errorparams->courseid = $value->id;
                        $errorparams->userid = $enrolment['userid'];
                        throw new moodle_exception('wscannotenrol', 'enrol_manual', '', $errorparams);
                    }

                    // Finally proceed the enrolment.
                    $enrolment['timestart'] = isset($enrolment['timestart']) ? $enrolment['timestart'] : 0;
                    $enrolment['timeend'] = isset($enrolment['timeend']) ? $enrolment['timeend'] : 0;
                    $enrolment['status'] = (isset($enrolment['suspend']) && !empty($enrolment['suspend'])) ?
                            ENROL_USER_SUSPENDED : ENROL_USER_ACTIVE;

                    $enrol->enrol_user($instance, $enrolment['userid'], $enrolment['roleid'],
                            $enrolment['timestart'], $enrolment['timeend'], $enrolment['status']);

                    //$transaction->allow_commit();
                }
                }
              

            } else {

                // Ensure the current user is allowed to run this function in the enrolment context.
                $context = context_course::instance($enrolment['courseid'], IGNORE_MISSING);
                //self::validate_context($context);

                // Check that the user has the permission to manual enrol.
                require_capability('enrol/manual:enrol', $context);

                // Throw an exception if user is not able to assign the role.
                $roles = get_assignable_roles($context);
                if (!array_key_exists($enrolment['roleid'], $roles)) {
                    $errorparams = new stdClass();
                    $errorparams->roleid = $enrolment['roleid'];
                    $errorparams->courseid = $enrolment['courseid'];
                    $errorparams->userid = $enrolment['userid'];
                    throw new moodle_exception('wsusercannotassign', 'enrol_manual', '', $errorparams);
                }

                // Check manual enrolment plugin instance is enabled/exist.
                $instance = null;
                $enrolinstances = enrol_get_instances($enrolment['courseid'], true);
                foreach ($enrolinstances as $courseenrolinstance) {
                  if ($courseenrolinstance->enrol == "manual") {
                      $instance = $courseenrolinstance;
                      break;
                  }
                }
                if (empty($instance)) {
                  $errorparams = new stdClass();
                  $errorparams->courseid = $enrolment['courseid'];
                  throw new moodle_exception('wsnoinstance', 'enrol_manual', $errorparams);
                }

                // Check that the plugin accept enrolment (it should always the case, it's hard coded in the plugin).
                if (!$enrol->allow_enrol($instance)) {
                    $errorparams = new stdClass();
                    $errorparams->roleid = $enrolment['roleid'];
                    $errorparams->courseid = $enrolment['courseid'];
                    $errorparams->userid = $enrolment['userid'];
                    throw new moodle_exception('wscannotenrol', 'enrol_manual', '', $errorparams);
                }

                // Finally proceed the enrolment.
                $enrolment['timestart'] = isset($enrolment['timestart']) ? $enrolment['timestart'] : 0;
                $enrolment['timeend'] = isset($enrolment['timeend']) ? $enrolment['timeend'] : 0;
                $enrolment['status'] = (isset($enrolment['suspend']) && !empty($enrolment['suspend'])) ?
                        ENROL_USER_SUSPENDED : ENROL_USER_ACTIVE;

                $enrol->enrol_user($instance, $enrolment['userid'], $enrolment['roleid'],
                        $enrolment['timestart'], $enrolment['timeend'], $enrolment['status']);

            

                //$transaction->allow_commit();
            }
        }

        return enrol_get_users_courses($enrolments[0]['userid'],false, 'id,fullname');  
    }

    /**
     *  Return structure for enrol_allusers().
     *
     * @return  all enrolled courses.
     * @since Moodle 2.2
     */
    public static function enrol_allusers_returns() {
        return new external_multiple_structure(
                    new external_single_structure(
                    array(
                        'id' => new external_value(PARAM_INT, 'course id'),
                        'fullname' => new external_value(PARAM_RAW, 'course name'),

                    ), 'information about enrolled courses'
                )
                
            
        );
    }

 
}