
<?php
require_once(dirname(__FILE__) . '../../lms_webservice.php');
require_once("{$CFG->dirroot}/lib/questionlib.php");
class tutor_webservice_questions extends lms_webservice {

    /**
     * Recieves tagid
     * @return \external_function_parameters
     */
    public static function question_tag_parameters() {
        return new external_function_parameters(
            array(
                'categoryname' => new external_value(PARAM_RAW,'To get category name',VALUE_DEFAULT,null), 
                'coursename' => new external_value(PARAM_RAW,'To get course name',VALUE_DEFAULT,null),
                'tags' => new external_single_structure(
                            array('name' => new external_value(PARAM_RAW, 'List of tag names.', VALUE_OPTIONAL)),
                                 'tags - operator OR is used', VALUE_DEFAULT, array())
            ));
    }

    /**
     * Tag with all questions
     *
     * @param string
     * @param string
     * @param string
     * @return array
     * @since Moodle 2.2
     */
    public static function question_tag($category = null, $course = null, $tags = array()) {
        global $DB,$CFG;
        $tagarr = $coursearr = '';
        $questioninfo = $simpleformat = $tagdata = $tagdatanew = $count = array();
        $params = self::validate_parameters(self::question_tag_parameters(), array('categoryname' => $category,'coursename' => $course,'tags' => $tags));
        if(!empty($course)){
		$iscourse = $DB->get_record('course',array('shortname' => $course),'id,shortname');
		if(!empty($iscourse)){
	        	$course_question = self::get_course_questions($course);	
		}else{
                    return 'Error : Course name : { '.$course .' } does not exists';
                }
        }
        if(!empty($category)){
		$iscategory = $DB->get_record('course_categories',array('name' => $category),'id,name');
		if(!empty($iscategory)){
	        	$category_question = self::get_category_questions($category);	
		}else{
                    return 'Error : Category name : { '.$category .' } does not exists';
                }
        }
        /*if(!empty($category)){
            $category_question = self::get_category_questions($category);
        }*/
        
        //$tags  array('tagname')
        if (!empty($params['tags']['name'])) {
            $questionsfortags = self::get_all_taggedquestions($params['tags']['name']);
            //$category  category name 
            if(!empty($category) && empty($course)){
                foreach ($questionsfortags as $key => $tquestion) {
                    if(in_array($tquestion, $category_question)){
                        $finalquestion []=$tquestion; 
                    }
                }
                if(!empty($finalquestion)){
                    return self::get_return_questions($finalquestion);
                }else{
                    return 'Error : No questions found.';
                }
            }
            //$course    course shortname
            if(!empty($course)){
                    foreach ($questionsfortags as $key => $question) {
                        if(in_array($question, $course_question)){
                            $finalquestion []=$question; 
                        }
                    }
                    if(!empty($finalquestion)){
                        return self::get_return_questions($finalquestion);
                    }else{
                    	return 'Error : No questions found.';
                    }
            }
            else{
                return self::get_return_questions($questionsfortags);
            }
                
        }
    	//$tags  array(null)
        else{
            //$category  category name 
            if(!empty($category) && empty($course)){
                return self::get_return_questions($category_question);
            }
            //$course  course shortname
            else{
                if(!empty($course)){
                    return self::get_return_questions($course_question);
                }else{
                    return 'Error : Enter either Course name {coursename} or Category name {categoryname}';
                }
            }
        }
      
    }
    /**
    * Returns description of method result value
    *
    * @return external_description
    * @since Moodle 2.2
    */
    public static function question_tag_returns() {
        return new external_single_structure(
            array(
            'question' => new external_multiple_structure(
                    new external_single_structure(
                    array(
                'id' => new external_value(PARAM_INT, 'question id'),
                'questiontext' => new external_value(PARAM_RAW, 'question text'),
                'answers' => new external_multiple_structure(
                        new external_single_structure(
                        array(
                    'id' => new external_value(PARAM_INT, 'answer id'),
                    'option' => new external_value(PARAM_RAW, 'option'),
                    'fraction' => new external_value(PARAM_RAW, 'fraction'),
                        )
                        ), 'answers', VALUE_OPTIONAL),
                'course' => new external_value(PARAM_RAW, 'course name'),
                'category' => new external_value(PARAM_RAW, 'category name'),
                'tags' => new external_value(PARAM_RAW, 'tags name')
                    )
                    ), 'question', VALUE_OPTIONAL)
            )
        );
    }
    /**
    * Returns questions based on tagnames
    * @param string  
    * @return array
    */
    public static function get_all_taggedquestions($tagspassed){
        global $DB;
        $tagarr = '';
        $tagsext = explode(',',$tagspassed);
        foreach($tagsext as $id => $tag){
            $tagarr .= "'".$tag."',"; 
        }
        $tagarr = rtrim($tagarr,",");
        $tagcount = count($tagsext);
        $tagquestions = $DB->get_records_sql('SELECT qs.id,qs.name,qs.questiontext,qs.qtype,qs.defaultmark,qs.penalty,ti.contextid FROM {question} qs 
                                              JOIN {tag_instance} ti ON ti.itemid = qs.id
                                              JOIN  {tag} tg ON tg.id = ti.tagid 
                                              AND tg.name IN ('.$tagarr.')');
        if(!empty($tagquestions)){
            foreach ($tagquestions as $tagqs => $tagquestion) {
                $tagdatanew = self::get_tagsof_questions($tagquestion->id);
                foreach ($tagsext as $tagsvalue) {
                    if(in_array($tagsvalue, $tagdatanew)){
                        $count[] = $tagsvalue; 
                    }
                }
                if($tagcount == count($count)){
                    unset($tagdatanew);
                    $tagquestionsfinal[$tagquestion->id] = $tagquestion;
                }else{
                    unset($tagdatanew);
                }
                unset($count);
            }
        }
        if(!empty($tagquestionsfinal)){
            return $tagquestionsfinal;
        }else{ return 'No question found';}
    }
    /**
    * Returns tags of a question 
    * @param int  
    * @return array
    */
    public static function get_tagsof_questions($qid){
        global $DB;
        $tagdatanews = array();
        $tagofqs = $DB->get_records_sql('SELECT tg.name FROM {tag} tg 
                                        JOIN {tag_instance} ti ON ti.tagid = tg.id
                                        JOIN {question} qs ON qs.id = ti.itemid 
                                        WHERE qs.id = '.$qid);
            foreach ($tagofqs as $tagofq) {
                $tagdatanews[] = $tagofq->name; 
            }
            return $tagdatanews;
    }
    /**
    * Returns questions for a course 
    * @param string 
    * @return array
    */
    public static function get_course_questions($course){
            global $DB;
            $courseselect = $DB->get_record('course',array('shortname'=>$course),'id,shortname');
            $course_questions = $DB->get_records_sql('SELECT q.id,q.name,q.questiontext,q.qtype,q.defaultmark,q.penalty,qc.contextid FROM {question} q 
                                            JOIN {question_categories} qc ON qc.id = q.category  
                                            JOIN {context} ct ON ct.id = qc.contextid 
                                            JOIN {course} c ON c.id = ct.instanceid 
                                            WHERE ct.contextlevel = 50 AND c.id='.$courseselect->id);  
            return $course_questions;
        
    }
    /**
    * Returns questions for a category 
    * @param string 
    * @return array
    */
    public static function get_category_questions($category){
            global $DB;
            $catselect = $DB->get_record('course_categories',array('name'=>$category,'visible'=>1),'id,name,parent');
            $courselists = get_courses($catselect->id, 'c.sortorder ASC', 'c.id, c.shortname, c.fullname');
            foreach($courselists as $id => $courselist){
                $coursearr .= "'".$courselist->id."',"; 
            }
            $coursearr = rtrim($coursearr,",");
            $category_questions = $DB->get_records_sql('SELECT q.id,q.name,q.questiontext,q.qtype,q.defaultmark,q.penalty,qc.contextid FROM {question} q 
                                            JOIN {question_categories} qc ON qc.id = q.category  
                                            JOIN {context} ct ON ct.id = qc.contextid 
                                            JOIN {course} c ON c.id = ct.instanceid 
                                            WHERE ct.contextlevel = 50 AND c.id IN ('.$coursearr.')');
            return $category_questions;
    }
    /**
    * Returns questions objects for a question 
    * @param string 
    * @return array
    */
    public static function get_return_questions($question_objects){
        global $DB;
        $options = get_question_options($question_objects);
        foreach($question_objects as $slotid=>$qs){
                $courseinstance = $DB->get_record('context',array('id'=>$qs->contextid,'contextlevel'=>'50'),'instanceid');
                $courseofqs = $DB->get_record('course',array('id' => $courseinstance->instanceid),'shortname,category');
                $categoryofqs = $DB->get_record('course_categories',array('id' => $courseofqs->category),'name');
                $tagsofqs = implode(',',self::get_tagsof_questions($qs->id));
                $messagetext = question_rewrite_question_urls($qs->questiontext, 'pluginfile.php', $qs->contextid, 'question', 'questiontext', array($slotid), $qs->id);
                $simpleformat[$qs->id] = array(
                    'id' => $qs->id,
                    'questiontext' => $messagetext,
                    'answers' => array(),
                    'course' => $courseofqs->shortname,
                    'category' => $categoryofqs->name,
                    'tags' => $tagsofqs
                );
            foreach ($qs->options->answers as $ans) {
                    $messagetext = question_rewrite_question_urls($ans->answer, 'pluginfile.php', $qs->contextid, 'question', 'answer', array($slotid), $ans->id);
                    $simpleformat[$qs->id]['answers'][$ans->id] = array('id' => $ans->id, 'option' => $messagetext, 'fraction' => intval($ans->fraction));
            }
        }
        $response = array(
            'question' => $simpleformat
        );
        return $response;
    }

}

