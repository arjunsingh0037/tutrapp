<?php

require_once(dirname(__FILE__).'/../lms_webservice.php');

class tutor_webservice_course extends lms_webservice {

    /**
     * Returns description of method parameters
     *
     * @return external_function_parameters
     * @since Moodle 2.3
     */
    public static function get_all_tags_parameters() {
        return new external_function_parameters(
                array('options' => new external_single_structure(
                    array('ids' => new external_multiple_structure(
                        new external_value(PARAM_INT, 'tag id')
                        , 'List of tag id. If empty return all tags.', VALUE_OPTIONAL)
                    ), 'options - operator OR is used', VALUE_DEFAULT, array())
                )
        );
    }

    /**
     * Get tags
     *
     * @param array $options It contains an array (list of ids)
     * @return array
     * @since Moodle 2.2
     */
  
     public static function get_all_tags($options = array()) {
        global $CFG, $PAGE, $DB;

        // Validate and normalize parameters.
        $params = self::validate_parameters(self::get_all_tags_parameters(), array('options' => $options));

        $systemcontext = context_system::instance();
        self::validate_context($systemcontext);

        // $canmanage = has_capability('moodle/tag:manage', $systemcontext);
        // $canedit = has_capability('moodle/tag:edit', $systemcontext);

        // $return = array();
        // $warnings = array();

        if (empty($CFG->usetags)) {
            throw new moodle_exception('tagsaredisabled', 'tag');
        }

       

        if (!array_key_exists('ids', $params['options'])
                or empty($params['options']['ids'])) {
            $tagobject = $DB->get_records('tag');
        } else {
            $tagobject = $DB->get_records_list('tag', 'id', $params['options']['ids']);
        }

        $return = array();
        foreach ($tagobject as  $value) {

            $return[] = array('id' => $value->id,'name'=> $value->name);
         
        }
        
        return $return;
    }

    /**
     * Return structure for get_all_tags()
     *
     * @return external_description
     */
    public static function get_all_tags_returns() {
        return new external_multiple_structure(
                    new external_single_structure(
                    array(
                        'id' => new external_value(PARAM_INT, 'tag id'),
                        // 'tagcollid' => new external_value(PARAM_INT, 'tag collection id'),
                        'name' => new external_value(PARAM_TAG, 'name'),
                        // 'rawname' => new external_value(PARAM_RAW, 'tag raw name (may contain capital letters)'),
                        // 'description' => new external_value(PARAM_RAW, 'tag description'),
                        // 'descriptionformat' => new external_format_value(PARAM_INT, 'tag description format'),
                        // 'flag' => new external_value(PARAM_INT, 'flag', VALUE_OPTIONAL),
                        // 'official' => new external_value(PARAM_INT,
                        //     'whether this flag is standard (deprecated, use isstandard)', VALUE_OPTIONAL),
                        // 'isstandard' => new external_value(PARAM_INT, 'whether this flag is standard', VALUE_OPTIONAL),
                        // 'viewurl' => new external_value(PARAM_URL, 'URL to view'),
                    ), 'information about all tag'
                )
                
            
        );
    }

   }