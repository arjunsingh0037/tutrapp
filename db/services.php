<?php

// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Web service local plugin template external functions and service definitions.
 *
 * @package    localtutorws
 * @copyright  2011 Jerome Mouneyrac
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// We defined the web service functions to install.
$functions = array(
        'tutorws_get_all_tags' => array(
                'classname'   => 'tutor_webservice_course',
                'methodname'  => 'get_all_tags',
                'classpath'   => '/local/tutorws/tag/tag.php',
                'description' => 'Return all tags',
                'type'        => 'read',
        ),
         'tutorws_get_all_categorywithcourse' => array(
                'classname'   => 'tutor_webservice_category',
                'methodname'  => 'get_all_categorywithcourse',
                'classpath'   => '/local/tutorws/course/course.php',
                'description' => 'Return all category with corresponding courses',
                'type'        => 'read',
        ),
        'tutorws_enrol_allusers' => array(
                'classname'   => 'tutor_webservice_enrol',
                'methodname'  => 'enrol_allusers',
                'classpath'   => '/local/tutorws/enrol/enrol.php',
                'description' => 'Return all enrolled courses  ',
                'type'        => 'read',
        ),
        'tutorws_get_videourl' => array(
                'classname'   => 'tutor_webservice_category',
                'methodname'  => 'get_videourl',
                'classpath'   => '/local/tutorws/course/course.php',
                'description' => 'Return all videourl with corresponding courses,tags,category',
                'type'        => 'read',
        ),
        'tutorws_get_questions'    => array(
                'classname'   => 'tutor_webservice_questions',
                'methodname'  => 'question_tag',
                'classpath'   => '/local/tutorws/tag/question_tag.php',
                'description' => 'Returns all questions based on the category,course and tags passed as parameter',
                'type'        => 'read',      
        )
        
        
       
);

// We define the services to install as pre-build services. A pre-build service is not editable by administrator.
$services = array(
        'My service' => array(
                'functions' => array_keys($functions),
                'restrictedusers' => 0,
                'enabled'=>1,
        )
);
