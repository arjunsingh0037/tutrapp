<?php

require_once(dirname(__FILE__).'/../lms_webservice.php');

class tutor_webservice_category extends lms_webservice {

    /**
     * Returns description of method parameters
     *
     * @return external_function_parameters
     * @since Moodle 2.3
     */
    public static function get_all_categorywithcourse_parameters() {
        return new external_function_parameters(
                array());

    }

    /**
     * Get  category with courses
     *
     * @param empty
     * @return array
     * @since Moodle 2.2
     */
  
     public static function get_all_categorywithcourse() {
        global $CFG, $PAGE, $DB;

        $systemcontext = context_system::instance();
        self::validate_context($systemcontext);
        $data = array();
        $resultset = $DB->get_records('course_categories');
        if ($resultset) {
            foreach ($resultset as $id => $row) {
                
                    $data[$row->id] = array(
                         'id' => $row->id,
                         'name' => $row->name,
                        'courses' => get_courses($row->id, '', 'c.id,c.fullname,c.shortname'),
                    );
                   
                
            }
        }
        
        return  $data;
    }

    /**
     * Return structure for get_all_categorywithcourse()
     *
     * @return external_description
     */
    public static function get_all_categorywithcourse_returns() {
        return new external_multiple_structure(
                    new external_single_structure(
                    array(
                        'id' => new external_value(PARAM_INT, 'category id'),
                        'name' => new external_value(PARAM_TAG, 'category name'),
                        'courses' => new external_multiple_structure(
                            new external_single_structure(
                            array(
                            'id' => new external_value(PARAM_INT, 'course id'),
                            'fullname' => new external_value(PARAM_RAW, 'course fullname'),
                            'shortname' => new external_value(PARAM_RAW, 'course shortname'),
                            )
                            ),'information about category and courses')

                    ), 'information about category and courses'
                )
                
            
        );
    }

       /**
     * Returns description of method parameters
     *
     * @return external_function_parameters
     * @since Moodle 2.3
     */
    public static function get_videourl_parameters() {

        return new external_function_parameters(
                array(
            'category' => new external_value(PARAM_RAW,'category name',VALUE_DEFAULT, null),
            'course' => new external_value(PARAM_RAW, 'course name',VALUE_DEFAULT, null),
             'tag' => new external_single_structure(
                    array('tags' => new external_value(PARAM_RAW, 'tag name', VALUE_OPTIONAL)),
                    'options - operator OR is used', VALUE_DEFAULT, array())
        ));
    }

    /**
     * Get videourl
     *
     * @param string $category optional
     * @param string $course optional
     * @param array $tag optional
     * @return array
     * @since Moodle 2.2
     */

    public static function get_videourl($category = null,$course = null,$tag = array()) {
        global $DB,$CFG;
        $paramsnw = self::validate_parameters(self::get_videourl_parameters(), array('category' => $category,'course' => $course,'tag' => $tag));
        // check if param course empty and param tag not empty.

        if(empty($paramsnw['course']) && !empty($paramsnw['tag']['tags']) && empty($paramsnw['category'])  ) {
 
           $finallrs = self::search_urlbytag($paramsnw['tag']['tags']);
           return $finallrs;
        }
        // check if param course not empty and param tag not empty.

        else if(!empty($paramsnw['course']) && !empty($paramsnw['tag']['tags']) && empty($paramsnw['category']) ) {

           $finallrs = self::search_urlbycoursetag($paramsnw['course'],$paramsnw['tag']['tags']);
           return $finallrs;
        }
        // check if param course not empty.
        else if(!empty($paramsnw['course']) && empty($paramsnw['tag']['tags']) && empty($paramsnw['category']) ) {

           $finallrs = self::search_urlbycourse($paramsnw['course']);
           return $finallrs;
        }

       // check if param catagory not empty .
        else if(!empty($paramsnw['category'])  && empty($paramsnw['tag']['tags']) ) {
            $resultset = $DB->get_record('course_categories',array('name' => $paramsnw['category']),'id');
            $allcoures  = get_courses($resultset->id, '', 'c.id,c.fullname,c.shortname');
            foreach ($allcoures as  $value) {
                $courseid[] = $value->id;
            }
            $finallrs = self::search_urlbycourse($courseid);
           return $finallrs;
        }
        // check if param catagory not empty and param tag not empty.
        else if(!empty($paramsnw['category'])  && !empty($paramsnw['tag']['tags'])) {
            $resultset = $DB->get_record('course_categories',array('name' => $paramsnw['category']),'id');
            $allcoures  = get_courses($resultset->id, '', 'c.id,c.fullname,c.shortname');
            foreach ($allcoures as  $value) {
                $courseid[] = $value->id;
            }
            $finallrs = self::search_urlbycoursetag($courseid,$paramsnw['tag']['tags']);
           return $finallrs;
          
        }
    }

    /**
     * Return structure for get_videourl()
     *
     * @return external_description
     */
      public static function get_videourl_returns() {
       return new external_multiple_structure(
                    new external_single_structure(
                    array(
                        'id' => new external_value(PARAM_INT, 'vediourl id'),
                        'name' => new external_value(PARAM_RAW, 'vediourl name'),
                        'externalurl' => new external_value(PARAM_RAW, 'externalurl'),
                        'tag' => new external_single_structure(
                        array('name' => new external_value(PARAM_RAW, 'tag name'))
                        )

                   )

                    ), 'information about vediourl'
                
                
            
        );
    }

   /**
     * Get videourl id,name,externalurl
     *
     * @param array $tag require
     * @return array
     * @since Moodle 2.2
     */
 public static function  search_urlbytag($tags){
    global $DB;
        $tagdatas = array();
        $tagdatanew = array();
         $count =array();
        $extags = explode( ',', $tags );
        $tagstr = "";

        foreach ($extags as $tag) {
            $tagstr .= "'".$tag."',";
        }

        $tagcout = count($extags);
        //print_r($tagcout);
        $tagstr = rtrim($tagstr, ',');
        $tagall = $DB->get_records_sql("SELECT DISTINCT ul.id,ul.name, ul.externalurl,ul.course  FROM {tag} tg LEFT JOIN {tag_instance} tgi ON tg.id = tgi.tagid
        LEFT JOIN {course_modules} cm ON cm.id = tgi.itemid LEFT JOIN {url} ul ON ul.id = cm.instance WHERE cm.module = 20 AND tg.name  IN ($tagstr)");
        if(!empty($tagall)) {
            foreach ($tagall as  $tagalls) {
               $tagdatas['id'] = $tagalls->id;
               $tagdatas['name'] = $tagalls->name;
               $tagdatas['externalurl'] = $tagalls->externalurl;
               $resultcourse = $DB->get_record('course', array('id' => $tagalls->course),'shortname,category');
               $resultsetcat = $DB->get_record('course_categories',array('id' => $resultcourse->category),'name');
               $result = $DB->get_records_sql('SELECT tg.name FROM {course_modules} cm LEFT JOIN {tag_instance} ti ON cm.id = ti.itemid  LEFT JOIN {tag} tg ON tg.id = ti.tagid WHERE cm.instance = "'.$tagdatas['id'].'" AND cm.module = 20');
                foreach ($result as  $value) {
                       $tagdatanew[] = $value->name;
                }
                foreach ($extags as $value1) {
                   if(in_array($value1, $tagdatanew)){

                    $count[] = $value1;
                   
                   }
                }
                  
                if($tagcout == count($count)){
               
                    $comma_separated = implode(",", $tagdatanew);
                    unset($tagdatanew);
                    $tagdatas['tag']['name'] = $resultsetcat->name.','.$resultcourse->shortname.','.$comma_separated;
                    $tagfinall[] = $tagdatas;

                } else {

                    unset($tagdatanew);

                }
                
                unset($count); 
            }
        }

        if(!empty($tagfinall)){

            return $tagfinall;

        } else {

            $tagdatas['id'] = Null;
            $tagdatas['name'] = 'No videourl found for this tag';
            $tagdatas['externalurl'] = 'No videourl found for this tag';
            $tagdatas['tag']['name'] = 'No videourl found for this tag';
            $tagfinall[] = $tagdatas;

            return $tagfinall;


        }
                   
       
    }
    /**
     * Get videourl id,name,externalurl
     *
     * @param array or string $course require
     * @return array
     * @since Moodle 2.2
     */
    public static function  search_urlbycourse($course){
        global $DB;
        $tagdatas = array();
        if(is_string($course)){
            
            $resultset = $DB->get_record('course', array('shortname' => $course),'id,fullname,category');
            
            $tagall = $DB->get_records_sql('SELECT DISTINCT ul.id,ul.name, ul.externalurl  FROM {tag} tg LEFT JOIN {tag_instance} tgi ON tg.id = tgi.tagid
            LEFT JOIN {course_modules} cm ON cm.id = tgi.itemid LEFT JOIN {url} ul ON ul.id = cm.instance WHERE cm.module = 20 AND ul.course = '.$resultset->id );
            if(!empty($tagall)){
                foreach ($tagall as  $tagalls) {
                   $tagdatas['id'] = $tagalls->id;
                   $tagdatas['name'] = $tagalls->name;
                   $tagdatas['externalurl'] = $tagalls->externalurl;

                   $resultsetcat = $DB->get_record('course_categories',array('id' =>  $resultset->category),'name');
                   $result = $DB->get_records_sql('SELECT tg.name FROM {course_modules} cm LEFT JOIN {tag_instance} ti ON cm.id = ti.itemid  LEFT JOIN {tag} tg ON tg.id = ti.tagid WHERE cm.instance = "'.$tagdatas['id'].'" AND cm.module = 20');
                    foreach ($result as  $value) {
                       $tagdatanew[] = $value->name;
                    }
                    $comma_separated = implode(",", $tagdatanew);
                    unset($tagdatanew);
                    $tagdatas['tag']['name'] = $resultsetcat->name.','.$course.','.$comma_separated;
                    $tagfinall[] = $tagdatas;
                }
                return $tagfinall;
            } else {

                $tagdatas['id'] = Null;
                $tagdatas['name'] = 'No videourl found for this tag';
                $tagdatas['externalurl'] = 'No videourl found for this tag';
                $tagdatas['tag']['name'] = 'No videourl found for this tag';
                $tagfinall[] = $tagdatas;

                return $tagfinall;

            }    
           


        }
        else if (is_array($course)){

            $coursesstr = " ";
            foreach ($course as $courses) {
                $coursesstr .= "".$courses.",";
            }
            $coursesstr = rtrim($coursesstr, ',');
            // print_r($coursesstr);
           
            $tagall = $DB->get_records_sql("SELECT DISTINCT ul.id,ul.name, ul.externalurl,ul.course  FROM {tag} tg LEFT JOIN {tag_instance} tgi ON tg.id = tgi.tagid
            LEFT JOIN {course_modules} cm ON cm.id = tgi.itemid LEFT JOIN {url} ul ON ul.id = cm.instance WHERE cm.module = 20
            AND ul.course IN ($coursesstr) ");
            if(!empty($tagall)){
                foreach ($tagall as  $tagalls) {
                   $tagdatas['id'] = $tagalls->id;
                   $tagdatas['name'] = $tagalls->name;
                   $tagdatas['externalurl'] = $tagalls->externalurl;
                   $resultcourse = $DB->get_record('course', array('id' => $tagalls->course),'shortname,category');
                   $resultsetcat = $DB->get_record('course_categories',array('id' => $resultcourse->category),'name');
                   $result = $DB->get_records_sql('SELECT tg.name FROM {course_modules} cm LEFT JOIN {tag_instance} ti ON cm.id = ti.itemid  LEFT JOIN {tag} tg ON tg.id = ti.tagid WHERE cm.instance = "'.$tagdatas['id'].'" AND cm.module = 20');
                    foreach ($result as  $value) {
                       $tagdatanew[] = $value->name;
                    }
                    $comma_separated = implode(",", $tagdatanew);
                    unset($tagdatanew);
                    $tagdatas['tag']['name'] = $resultsetcat->name.','.$resultcourse->shortname.','.$comma_separated;
                    $tagfinall[] = $tagdatas;
                }

                return $tagfinall;
                
            }  else {

                $tagdatas['id'] = Null;
                $tagdatas['name'] = 'No videourl found for this tag';
                $tagdatas['externalurl'] = 'No videourl found for this tag';
                $tagdatas['tag']['name'] = 'No videourl found for this tag';
                $tagfinall[] = $tagdatas;

                return $tagfinall;

            }    
                   
            
           
        }
      
    }

     /**
     * Get videourl id,name,externalurl
     *
     * @param array or string $course require
     * @param array $tag require
     * @return array
     * @since Moodle 2.2
     */

    public static function search_urlbycoursetag($course,$tags){
        global $DB;
        $tagdatas = array();
        $tagdatanew = array();
         $count =array();
        $extags = explode( ',', $tags );
        $tagstr = "";

        foreach ($extags as $tag) {
            $tagstr .= "'".$tag."',";
        }

        $tagcout = count($extags);

        $tagstr = rtrim($tagstr, ',');

        if(is_string($course)){
            $resultset = $DB->get_record('course', array('shortname' => $course),'id,fullname,category');
            $tagall = $DB->get_records_sql("SELECT DISTINCT ul.id,ul.name, ul.externalurl  FROM {tag} tg LEFT JOIN {tag_instance} tgi ON tg.id = tgi.tagid
            LEFT JOIN {course_modules} cm ON cm.id = tgi.itemid LEFT JOIN {url} ul ON ul.id = cm.instance WHERE cm.module = 20
            AND ul.course = $resultset->id  AND tg.name  IN ($tagstr)" );

            foreach ($tagall as  $tagalls) {
               $tagdatas['id'] = $tagalls->id;
               $tagdatas['name'] = $tagalls->name;
               $tagdatas['externalurl'] = $tagalls->externalurl;
               $resultsetcat = $DB->get_record('course_categories',array('id' =>  $resultset->category),'name');
               $result = $DB->get_records_sql('SELECT tg.name FROM {course_modules} cm LEFT JOIN {tag_instance} ti ON cm.id = ti.itemid  LEFT JOIN {tag} tg ON tg.id = ti.tagid WHERE cm.instance = "'.$tagdatas['id'].'" AND cm.module = 20');
                foreach ($result as  $value) {
                   $tagdatanew[] = $value->name;
            }
           
            foreach ($extags as $value1) {
               if(in_array($value1, $tagdatanew)){

                $count[] = $value1;
               
               }
            }
                
            if($tagcout == count($count)){
           
                $comma_separated = implode(",", $tagdatanew);
                unset($tagdatanew);
                $tagdatas['tag']['name'] = $resultsetcat->name.','.$course.','.$comma_separated;
                $tagfinall[] = $tagdatas;


            } else {

                unset($tagdatanew);

            }
            
            unset($count); 
            }
                   
            if(!empty($tagfinall)){

                return $tagfinall;

            } else {

                $tagdatas['id'] = Null;
                $tagdatas['name'] = 'No videourl found for this tag';
                $tagdatas['externalurl'] = 'No videourl found for this tag';
                $tagdatas['tag']['name'] = 'No videourl found for this tag';
                $tagfinall[] = $tagdatas;

                return $tagfinall;


            }


        }
        else if (is_array($course)){
            $coursesstr = "";
            foreach ($course as $courses) {
                $coursesstr .= "".$courses.",";
            }
            $coursesstr = rtrim($coursesstr, ',');
            $tagall = $DB->get_records_sql("SELECT DISTINCT ul.id, ul.name, ul.externalurl,ul.course   FROM {tag} tg LEFT JOIN {tag_instance} tgi ON tg.id = tgi.tagid
            LEFT JOIN {course_modules} cm ON cm.id = tgi.itemid LEFT JOIN {url} ul ON ul.id = cm.instance WHERE cm.module = 20
            AND ul.course IN ($coursesstr)  AND tg.name  IN ($tagstr)");

            foreach ($tagall as  $tagalls) {
               $tagdatas['id'] = $tagalls->id;
               $tagdatas['name'] = $tagalls->name;
               $tagdatas['externalurl'] = $tagalls->externalurl;
               $resultcourse = $DB->get_record('course', array('id' => $tagalls->course),'shortname,category');
               $resultsetcat = $DB->get_record('course_categories',array('id' => $resultcourse->category),'name');
               $result = $DB->get_records_sql('SELECT tg.name FROM {course_modules} cm LEFT JOIN {tag_instance} ti ON cm.id = ti.itemid  LEFT JOIN {tag} tg ON tg.id = ti.tagid WHERE cm.instance = "'.$tagdatas['id'].'" AND cm.module = 20');
                foreach ($result as  $value) {
                   $tagdatanew[] = $value->name;
                }
           
            foreach ($extags as $value1) {
               if(in_array($value1, $tagdatanew)){

                $count[] = $value1;
               
               }
            }
              
            if($tagcout == count($count)){
           
                $comma_separated = implode(",", $tagdatanew);
                unset($tagdatanew);
                $tagdatas['tag']['name'] = $resultsetcat->name.','.$resultcourse->shortname.','.$comma_separated;
                $tagfinall[] = $tagdatas;


            } else {

                unset($tagdatanew);

            }
            
            unset($count); 
            }
                   
            if(!empty($tagfinall)){

                return $tagfinall;

            } else {

                $tagdatas['id'] = Null;
                $tagdatas['name'] = 'No videourl found for this tag';
                $tagdatas['externalurl'] = 'No videourl found for this tag';
                $tagdatas['tag']['name'] = 'No videourl found for this tag';
                $tagfinall[] = $tagdatas;

                return $tagfinall;


            }
        }
      
    }

}